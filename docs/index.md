# 시계열 분석과 예측 <sup>[1](#footnote_1)</sup>

> <font size="3">다양한 기법을 사용하여 시계열 분석과 예측을 수행하는 방법에 대해 알아본다.</font>

## 목차

1. [개요](./time-series-analysis-and-forecasting.md#intro)
1. [시계열 데이터란?](./time-series-analysis-and-forecasting.md#sec_02)
1. [시계열의 탐색적 데이터 분석](./time-series-analysis-and-forecasting.md#sec_03)
1. [시계열 분해](./time-series-analysis-and-forecasting.md#sec_04)
1. [정상성과 차이](./time-series-analysis-and-forecasting.md#sec_05)
1. [자기상관과 편 자기상관](./time-series-analysis-and-forecasting.md#sec_06)
1. [ARIMA 모델](./time-series-analysis-and-forecasting.md#sec_07)
1. [계절별 ARIMA 모델](./time-series-analysis-and-forecasting.md#sec_08)
1. [지수 평활 방법](./time-series-analysis-and-forecasting.md#sec_09)
1. [예언자](./time-series-analysis-and-forecasting.md#sec_10)
1. [시계열 신경망](./time-series-analysis-and-forecasting.md#sec_11)
1. [시계열 예측을 위한 평가지표](./time-series-analysis-and-forecasting.md#sec_12)
1. [마치며](./time-series-analysis-and-forecasting.md#summary)

<a name="footnote_1">1</a>: [ML Tutorial 29 — Time Series Analysis and Forecasting](https://levelup.gitconnected.com/ml-tutorial-29-time-series-analysis-and-forecasting-cefe7dd8d1f8?sk=01966f8d03468f41ca9a3439abe6a2c6)를 편역하였습니다.
