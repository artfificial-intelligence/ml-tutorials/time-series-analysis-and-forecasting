# 시계열 분석과 예측

## <a name="intro"></a> 개요
Python을 이용한 시계열 분석과 예측에 관한 이 포스팅에서는 ARIMA, 지수 평활화, 예언자, 신경망 등 다양한 기법을 사용하여 시계열 분석과 예측을 수행하는 방법을 설명할 것이다. 또한 다양한 메트릭을 사용하여 예측의 성능을 평가하는 방법도 다룰 것이다.

시계열 분석은 시간의 흐름에 따른 순차적 자료의 패턴과 추세를 연구하고 모형화하는 과정이다. 시계열 예측은 시계열 분석을 적용하여 과거와 현재의 관측치를 기반으로 자료의 미래 가치를 예측하는 것이다. 시계열 분석과 예측은 경제, 금융, 경영, 공학, 과학 등 많은 분야에서 널리 활용되고 있다.

이 포스팅을 학습하면 끝날 다음 작업을 수행할 수 있다.

- 시계열 자료의 기본 개념과 속성 이해
- pandas, matplotlib, seaborn 등 Python 라이브러리를 이용하여 시계열 데이터의 탐색적 데이터 분석 수행
- 시계열 데이터를 추세, 계절성 및 잔차 성분으로 분해
- 차분과 변환 방법을 사용하여 시계열 데이터의 정상성의 테스트와 달성
- 시계열 데이터의 자기 상관과 부분 자기 상관 함수의 계산과 해석
- 통계 모델 라이브러리를 사용하여 시계열 데이터에 대한 ARIMA 모델 구축과 적합
- SARIMAX 클래스를 사용하여 계절 시계열 데이터를 처리할 수 있도록 ARIMA 모델 확장
= 통계 모형 라이브러리를 사용하여 단순, 이중 및 삼중 지수 평활 같은 지수 평활 방법을 시계열 데이터에 적용
- 페이스북이 개발한 라이브러리인 'Prophet'로 베이지안 접근법을 이용한 시계열 예측의 작성과 평가
- 순환 신경망(RNN)과 LSTM(Long Short-Term Memory) 네트워크 같은 신경망을 사용하여 TensorFlow와 Keras 라이브러리로 시계열 데이터의 모델링과 예측
- 평균 절대 오차(MAE), 평균 제곱 오차(RMSE), 평균 절대 오차(MAPE) 같은 지표를 사용하여 다양한 시계열 예측 방법의 정확도와 신뢰도를 비교 평가

이 포스팅을 따라 학습하려면 컴퓨터에 Python 3를 설치하고 다음 라이브러리를 설치해야 한다.

- pandas: 데이터 조작과 분석을 위한 라이브러리
- numpy: 과학 컴퓨팅과 선형 대수학을 위한 라이브러리
- matplotlib: 데이터 시각화와 플롯을 위한 라이브러리
- Seaborn: 통계 데이터 시각화를 위한 라이브러리
- statsmodels: 통계 모델링과 테스트를 위한 라이브러리
- Prophet: 시계열 예측을 위한 라이브러리
- Tensorflow: 기계 학습과 딥러닝을 위한 라이브러리
- Keras: 신경망 구축과 훈련을 위한 상위 수준 API

pip 명령을 사용하여 이러한 라이브러리를 설치할 수 있다.

```bash
# Install the required libraries
$ pip install pandas numpy matplotlib seaborn statsmodels prophet tensorflow keras
```

또는 이러한 라이브러리의 대부분이 미리 설치되어 있는 Anaconda와 같은 Python 배포판을 사용할 수 있다.

시계열 분석과 예측에 뛰어들 준비가 되었나요? 시작해 보겠다!

## <a name="sec_02"></a> 시계열 데이터란?
시계열 분석과 예측의 기법과 방법에 대해 자세히 살펴보기 전에 먼저 시계열 자료가 무엇이며, 왜 중요한지를 파악해 보자.

시계열 자료는 시간에 따라 순차적으로 기록된 관측치로 구성된 자료의 한 종류이다. 예를 들어, 시계열 자료는 주식의 일별 종가, 상품의 월별 판매량, 도시의 연간 기온, 가구의 시간당 전력 소비량 등을 모두 포함한다. 시계열 자료는 단변량일 수 있는데, 단변량은 하나의 변수만 포함한다는 의미이고, 다변량은 시간에 따라 측정되는 여러 변수를 포함한다는 의미이다.

시계열 자료가 왜 중요한가? 시계열 자료는 우리가 한 변수의 과거 행동과 패턴을 분석하고 그 정보를 사용하여 그 변수의 미래 가치를 예측할 수 있게 해주기 때문에 중요하다. 시계열 예측은 우리가 더 나은 결정을 내리고 예상되는 결과를 바탕으로 우리의 행동을 최적화하는 데 도움을 줄 수 있다. 예를 들어, 시계열 예측은 다음과 같이 우리를 도울 수 있다.

- 제품이나 서비스의 수요와 공급을 예측하고 그에 따라 재고와 가격을 조정한다
- 비즈니스의 수익과 비용을 예측하고 그에 따라 예산과 전략을 계획한다
- 시장의 동향과 움직임을 예측하고 그에 따라 투자허거나 거래한다.
- 자연적 또는 인공적 사건의 영향과 위험을 추정하고 그에 따라 준비하거나 완화시킨다.

하지만, 시계열 자료는 분석과 예측에 있어서도 도전과 어려움을 내포하고 있다. 시계열 자료는 추세, 계절성, 사이클, 소음, 이상치, 비고정성 등 다양한 요인에 의해 영향을 받아 복잡하고 예측이 불가능한 경우가 많다. 더욱이, 시계열 자료는 자료 생성 과정에서 내재된 무작위성과 소음으로 인해 불확실성과 변동성을 수반하는 경우가 많다. 따라서, 시계열 분석과 예측은 이러한 도전과 어려움을 처리하고 정확하고 신뢰할 수 있는 예측을 산출할 수 있는 전문적인 기법과 방법을 필요로 한다.

다음 절에서는 pandas, matplotlib, seaborn 등의 Python 라이브러리를 이용하여 시계열 데이터의 탐색적 데이터 분석을 수행하는 방법에 대해 알아본다.

## <a name="sec_03"></a> 시계열의 탐색적 데이터 분석
본 절에서는 pandas, matplotlib, seaborn 등의 Python 라이브러리를 이용하여 시계열 자료의 탐색적 자료 분석(EDA)을 수행할 것이다. EDA는 자료의 특성, 패턴, 분포 등을 이해하기 위해 자료를 탐색하고 시각화하는 과정이다. EDA는 자료에 대한 통찰력을 얻고, 잠재적인 문제나 이상 징후를 파악하며, 추가 분석과 모델링을 위해 자료를 준비하는 데 도움을 줄 수 있다.

EDA 프로세스를 설명하기 위해 1949년 1월부터 1960년 12월까지 한 항공사의 월간 승객 수를 포함하는 샘플 데이터 세트를 사용할 것이다. 데이터 세트는 [여기](https://raw.githubusercontent.com/jbrownlee/Datasets/master/airline-passengers.csv)에서 다운로드하거나 웹에서 다음 코드를 사용하여 로드할 수 있다.

```python
# Import pandas library
import pandas as pd
# Load the dataset from the web
url = "https://raw.githubusercontent.com/jbrownlee/Datasets/master/airline-passengers.csv"
df = pd.read_csv(url, index_col="Month", parse_dates=True)
```

데이터세트에는 두 개의 열 월과 승객수가 있다. 월 열은 dataframe의 인덱스이며 각 관측치의 날짜를 포함한다. 승객수 열은 대상(target) 변수이며 수천 명 단위로 항공사의 승객 수를  포함한다. `head()` 메서드를 사용하여 dataframe의 처음 다섯 행을 볼 수 있다.

```python
# View the first five rows of the dataframe
print(df.head())
```

출력은 다음과 같다.

```
Month       Passengers
1949-01-01  112
1949-02-01  118
1949-03-01  132
1949-04-01  129
1949-05-01  121
```

우리는 데이터프레임이 1949년 1월부터 1960년 12월까지 매달 하나씩 144 행을 가짐을 알 수 있다. 이를 확인하기 위해 형상 속성을 사용할 수 있다.

```python
# Check the shape of the dataframe
print(df.shape)
```

출력은 다음과 같다.

```
(144, 1)
```

또한 `info()` 메서드를 사용하여 데이터 타입, null이 아닌 값의 수, 메모리 사용량 등 데이터프레임에 대한 자세한 정보를 얻을 수 있다.

```python
# Get more information about the dataframe
print(df.info())
```

출력은 다음과 같다.

```
DatetimeIndex: 144 entries, 1949-01-01 to 1960-12-01
Data columns (total 1 columns):
 #   Column      Non-Null Count  Dtype
---  ------      --------------  -----
 0   Passengers  144 non-null    int64
dtypes: int64(1)
memory usage: 2.2 KB
```

데이터프레임에 결측값이 없고 승객수 열의 데이터 타입이 정수인 `int64`임을 알 수 있다. `describe()` 메서드를 사용하여 `count`, `standard deviation`, `minimum`, `maximum` 및 `quartile` 같은 승객수 열의 요약 통계량을 얻을 수도 있다.

```python
# Get summary statistics of the Passengers column
df.describe()
```

출력은 다음과 같다.

```
Passengers
count  144.000000
mean   280.298611
std    119.966317
min    104.000000
25%    180.000000
50%    265.500000
75%    360.500000
max    622.000000
```

우리는 항공사의 평균 탑승객 수가 약 280명이고, 표준편차는 약 120임을 알 수 있다. 최소 탑승객 수는 104명이고, 최대 탑승객 수는 622명이다. 탑승객의 중앙값은 265.5명으로 관측치의 절반이 이 값 이상이고 절반이 아래임을 의미한다. 25%와 75% 분위수는 각각 180과 360.5명으로 관측치의 25%가 180 이하이고 25%가 360.5 이상임을 의미한다.

이제 우리는 데이터에 대한 기본적인 이해를 마쳤으니, EDA의 다음 단계인 데이터 시각화로 넘어간다. 데이터 시각화는 데이터의 패턴, 추세 및 분포를 나타내기 위해 데이터의 그래픽 표현을 만드는 과정이다. 데이터 시각화는 데이터에 대한 더 많은 통찰력을 얻고, 잠재적인 문제 또는 이상 징후를 식별하고, 우리의 발견을 효과적으로 전달하는 데 도움을 줄 수 있다.

다음 절에서는 matplotlib와 seaborn 라이브러리를 이용하여 시계열 데이터에 대한 다양한 플롯과 도표를 작성하는 방법을 알아본다.

## <a name="sec_04"></a> 시계열 분해
이 절에서는 statsmodels 라이브러리를 이용하여 시계열 자료를 그 구성요소로 분해하는 방법을 설명한다. 시계열 분해는 시계열을 추세, 계절성, 주기, 잔차의 세 가지 또는 네 가지 구성요소로 분해하는 과정이다. 추세는 자료의 장기적 방향성, 계절성은 자료의 주기적 변동, 주기는 자료의 비주기적 변동, 잔차는 자료의 무작위 잡음이다. 시계열 분해는 자료에 영향을 미치는 근본적인 패턴과 요인을 이해하고 예측 정확도와 신뢰도를 향상시키는 데 도움을 줄 수 있다.

시계열 분해의 방법에는 크게 두 가지, 즉 덧셈법(additive method)과 곱셈법(multiplicative method)이 있다. 덧셈법은 시계열의 성분들이 함께 더해졌다고 가정하는 반면, 곱셈법은 시계열의 성분들이 함께 곱셈된다고 가정한다. 덧셈법은 진폭과 분산이 일정한 시계열에 적합한 반면, 곱셈법은 진폭과 분산이 증가하거나 감소하는 시계열에 적합하다. 덧셈법과 곱셈법의 일반적인 공식은 다음과 같다.

덧셈법: $\mathbf{Y}_t = \mathbf{T}_t + \mathbf{S}_t + \mathbf{C}_t + \mathbf{R}_t$

곱셈법: Yt = $\mathbf{Y}_t = \mathbf{T}_t * \mathbf{S}_t * \mathbf{C}_t * \mathbf{R}_t$

여기서 $\mathbf{Y}_t$는 원 시계열, $\mathbf{T}_t$는 추세 성분, $\mathbf{S}_t$는 계절성 성분, $\mathbf{C}_t$는 사이클 성분, $\mathbf{R}_t$는 잔차 성분이다.

Python에서 시계열 분해를 수행하기 위해서는 statsmodels 라이브러리에서 `seasonal_decomposse()` 함수를 사용할 수 있다. 이 함수는 시계열을 입력으로 받아 분해된 성분을 포함하는 객체를 반환한다. 분해 방법을 선택하기 위해서는 모형 모수를 `"additive"` 또는 `"multiplicative"`로 지정할 수 있다. 예를 들어, 덧셈법을 사용하여 항공사 승객 데이터세트를 분해하기 위한 코드는 다음과 같다.

```python
# Import statsmodels library
import statsmodels.api as sm

# Decompose the time series using the additive method
decomposition = sm.tsa.seasonal_decompose(df, model="additive")
# View the decomposed components
decomposition.plot()
```

<span style="color:red">출력 결과를 보일 것</span>

출력에서 시계열이 뚜렷한 상승 추세, 강한 계절적 패턴, 그리고 약간의 무작위 노이즈를 가지고 있음을 알 수 있다. 시계열이 규칙적인 주기나 변동을 가지고 있지 않기 때문에 주기 성분은 그다지 명확하지 않다. 분해 대상의 속성 추세, 계절, 주기 및 잔차(residual)를 사용하여 각 성분에 개별적으로 접근할 수도 있다. 예를 들어 추세 성분을 보기 위해 우리는 다음과 같은 코드를 사용할 수 있다.

```python
# View the trend component
decomposition.trend
```

출력은 각 월의 추세 값을 포함하는 데이터프레임이다.

```
Month       Trend
1949-01-01  NaN
1949-02-01  NaN
1949-03-01  118.333333
1949-04-01  120.333333
1949-05-01  122.666667
...         ...
1960-08-01  465.500000
1960-09-01  467.083333
1960-10-01  471.583333
1960-11-01  NaN
1960-12-01  NaN
```

`seasonal_decompose()` 함수는 추세를 추정하기 위해 이동 평균을 사용하고 처음과 마지막 관측치의 추세를 계산할 수 없기 때문에 시계열의 시작과 끝에 추세 성분이 일부 결측값을 가짐을 알 수 있다. matplotlib 라이브러리를 사용하여 각 성분을 별도로 표시할 수도 있다. 예를 들어 계절성 성분을 표시하기 위해 다음 코드를 사용할 수 있다.

```python
# Import matplotlib library
import matplotlib.pyplot as plt
# Plot the seasonality component
plt.figure(figsize=(10, 6))
plt.plot(decomposition.seasonal)
plt.title("Seasonality Component of Airline Passengers")
plt.xlabel("Month")
plt.ylabel("Seasonal Effect")
plt.show()
```

<span style="color:red">출력 결과를 보일 것</span>

출력에서 계절성 구성요소는 7월과 8월에 정점을, 11월과 2월에 저점을 갖는 뚜렷한 연도별 패턴을 보임을 알 수 있다. 이는 항공사의 승객 수가 휴일, 날씨, 관광 등 계절적 요인에 영향을 받음을 나타낸다.

이 절에서는 statsmodels 라이브러리를 이용하여 시계열 자료를 그 구성요소로 분해하는 방법을 보였다. 시계열 분해는 자료에 영향을 미치는 근본적인 패턴과 요인을 이해하고 예측 정확도와 신뢰도를 향상시키는 데 도움을 줄 수 있다. 다음 절에서는 차분법(differencing method)과 변환법(transformation method)을 이용하여 시계열 자료의 정상성을 검정하고 달성하는 방법을 설명할 것이다.

## <a name="sec_05"></a> 정상성과 차이
이 절에서는 차분법과 변환법을 이용하여 시계열 데이터의 정상성(stationarity)을 검정하고 달성하는 방법을 설명한다. 정상성은 시계열의 통계적 특성인 평균, 분산, 자기상관 등이 시간에 따라 변하지 않는다는 것을 의미하는 성질이다. 정상성은 시계열 분석과 예측에 중요한데, 이는 뒤 절에서 사용할 많은 방법과 모델들이 시계열이 정지해 있다고 가정하기 때문이다. 따라서 우리는 시계열이 정지해 있는지 확인해야 하며, 정지해 있지 않다면 여러 기법을 적용하여 정지해 있도록 해야 한다.

우리는 시계열이 정지해 있는지 어떻게 검정할 수 있을까? 한 가지 방법은 시계열을 표시하고 시간에 따른 수준, 추세 또는 계절성의 변화를 시각적으로 검사하는 것이다. 예를 들어, matplotlib 라이브러리를 사용하여 항공사 승객의 시계열을 표시하자.

```python
# Import matplotlib library
import matplotlib.pyplot as plt
# Plot the time series
plt.figure(figsize=(10, 6))
plt.plot(df)
plt.title("Airline Passengers")
plt.xlabel("Month")
plt.ylabel("Passengers (in thousands)")
plt.show()
```

<span style="color:red">출력 결과를 보일 것</span>

출력에서 시계열이 뚜렷한 상승 추세와 강한 계절 패턴을 가지고 있음을 알 수 있으며, 이는 시계열의 평균과 분산이 시간에 따라 일정하지 않다는 것을 의미한다. 따라서 시계열이 정상적이지 않다는 결론을 내릴 수 있다.

시계열의 정상성을 검정하는 또 다른 방법은 Augmented Dickey-Fuller(ADF) 검정과 같은 통계적 검정을 사용하는 것이다. ADF 검정은 단위근 검정의 일종으로 시계열에 단위근이 존재한다는 귀무가설을 검정하는 것으로 비정상적이라는 것을 의미한다. ADF 검정은 다음과 같은 회귀모형을 사용한다.

$\Delta \mathbf{Y}_t = \alpha + \beta t + \gamma \mathbf{Y}_{t-1} + \delta_{1} \mathbf{Y}_{t-1}+ ... + \delta_{p} \mathbf{Y}_{t-p} + \epsilon t$

여기서 $\mathbf{Y}_t$는 시계열, $\Delta$는 차분 연산자, $t$는 시간지수, $\alpha$는 절편, $\beta$는 시간추세의 계수, $\gamma$는 시계열의 시차수준의 계수, $\delta_{i}$는 시계열의 시차수준의 계수, $p$는 시차순서, $\epsilon t$는 오차항이다. ADF 검정통계량은 $\gamma$의 추정치이며 귀무가설은 $\gamma  = 0$으로 시계열이 단위근을 가지며 비정상임을 의미한다.

Python에서 ADF 검정을 수행하기 위해서는 statsmodels 라이브러리에서 a`dfuller()` 함수를 사용할 수 있다. 이 함수는 시계열을 입력으로 받아 ADF 검정 통계량, p-value와 서로 다른 신뢰 수준에 대한 임계값을 반환한다. 예를 들어 항공사 승객 시계열에 대한 ADF 검정을 수행하기 위해서는 다음 코드를 사용할 수 있다.

```python
# Import statsmodels library
import statsmodels.api as sm

# Perform the ADF test on the time series
result = sm.tsa.adfuller(df["Passengers"])
# Print the ADF test statistic, the p-value, and the critical values
print("ADF test statistic:", result[0])
print("p-value:", result[1])
print("Critical values:")
for key, value in result[4].items():
    print(key, value)
```

출력은 다음과 같다.

```
ADF test statistic: 0.8153688792060468
p-value: 0.9918802434376411
Critical values:
1% -3.4816817173418295
5% -2.8840418343195267
10% -2.578770059171598
```

ADF 검정통계량은 양의 값으로 임계값보다 훨씬 크며 p-value는 1에 가깝다는 것을 알 수 있다. 이는 시계열이 단위근을 가지며 비고정적이라는 귀무가설을 기각할 수 있음을 의미한다. 따라서 시계열이 정상적이지 않다는 것을 확인할 수 있다.

우리는 어떻게 시계열을 정상적 상태로 만들 수 있을까? 일반적인 한 기법은 차분을 시계열에 적용하는 것인데, 이것은 우리가 시계열의 현재 값을 이전 값에서 뺀다는 것을 의미한다. 차분은 시계열의 추세와 계절성 성분을 제거하고, 시계열을 더 정상 상태로 만드는 데 도움이 된다. 우리는 차분을 시계열에 적용하기 위해 pandas 라이브러리의 `diff()` 메서드를 사용할 수 있다. 예를 들어, 우리는 항공사 승객 시계열에 1차 차분을 적용하기 위해 다음과 같은 코드를 사용할 수 있다.

```python
# Apply first-order differencing to the time series
df_diff = df.diff()
# View the first five rows of the differenced time series
df_diff.head()
```

출력은 다음과 같다.

```
Month       Passengers
1949-01-01  NaN
1949-02-01  6.0
1949-03-01  14.0
1949-04-01  -3.0
1949-05-01  -8.0
```

우리는 첫 번째 관측치에 대한 차이를 계산할 수 없기 때문에, 미분된 시계열이 처음에 하나의 결측값을 갖는다는 것을 알 수 있다. 우리는 matplotlib 라이브러리를 이용하여 미분된 시계열을 플롯할 수도 있다.

```python
# Plot the differenced time series
plt.figure(figsize=(10, 6))
plt.plot(df_diff)
plt.title("Differenced Airline Passengers")
plt.xlabel("Month")
plt.ylabel("Passengers (in thousands)")
plt.show()
```

<span style="color:red">출력 결과를 보일 것</span>

출력에서 차분 시계열이 원래 시계열보다 추세와 계절성이 적고 안정적으로 보이는 것을 볼 수 있다. 차분 시계열에 대한 ADF 검정을 수행하여 정상성을 확인할 수도 있다.

```python
# Perform the ADF test on the differenced time series
result = sm.tsa.adfuller(df_diff["Passengers"].dropna())
# Print the ADF test statistic, the p-value, and the critical values
print("ADF test statistic:", result[0])
print("p-value:", result[1])
print("Critical values:")
for key, value in result[4].items():
    print(key, value)
```

출력은 다음과 같다.

```
ADF test statistic: -2.8292668241699874
p-value: 0.05421329028382711
Critical values:
1% -3.4816817173418295
5% -2.8840418343195267
10% -2.578770059171598
```

ADF 검정통계량은 음의 값으로 임계값에 가깝고 p-value는 0.05에 가깝다는 것을 알 수 있다. 이는 차분 시계열이 단위 근(unit root)을 가지며 5% 유의수준에서 비정상적이라는 귀무가설을 기각할 수 있음을 의미한다. 따라서 차분 시계열은 정상적이라는 결론을 내릴 수 있다.

시계열을 정상 상태로 만드는 또 다른 기법은 시계열에 변환을 적용하는 것인데, 이것은 우리가 시계열 값에 수학적 함수를 적용하는 것을 의미한다. 변환은 분산을 안정화시키고 시계열의 왜도를 감소시켜 더 정상 상태로 만드는 데 도움을 줄 수 있다. 우리는 다양한 수학적 함수를 제공하는 numpy 라이브러리를 이용하여 시계열에 변환을 적용할 수 있다. 예를 들어, 우리는 로그 변환을 항공사 승객 시계열에 적용하기 위해 다음과 같은 코드를 사용할 수 있다.

```python
# Import numpy library
import numpy as np

# Apply logarithmic transformation to the time series
df_log = np.log(df)
# View the first five rows of the transformed time series
df_log.head()
```

<span style="color:red">출력 결과를 보일 것</span>

## <a name="sec_06"></a> 자기상관과 편 자기상관
이 절에서는 차분법과 변환법을 이용하여 시계열 자료의 정상성을 검정하고 달성하는 방법을 설명한다. 정상성은 시계열의 통계적 특성인 평균, 분산, 자기상관 등이 시간에 따라 변하지 않는다는 것을 의미하는 성질이다. 정상성은 시계열 분석과 예측에 중요한데, 이는 뒤 절에서 사용할 많은 방법과 모델들이 시계열이 정성적이라고 가정하기 때문이다. 따라서 우리는 시계열이 정상적인지 확인해야 하며, 정상적이지 않다면 기법을 적용하여 정상적이 도록 만들어야 한다.

우리는 시계열이 정상적인지 어떻게 검정할 수 있을까? 한 방법은 시계열을 표시하고 시간에 따른 수준, 추세 또는 계절성의 변화를 시각적으로 검사하는 것이다. 예를 들어, matplotlib 라이브러리를 사용하여 항공사 승객의 시계열을 디스플레이한다.

```python
# Import matplotlib library
import matplotlib.pyplot as plt

# Plot the time series
plt.figure(figsize=(10, 6))
plt.plot(df)
plt.title("Airline Passengers")
plt.xlabel("Month")
plt.ylabel("Passengers (in thousands)")
plt.show()
```

<span style="color:red">이전 절과 내용이 동일함. 검토하여 수정하여야 함</span>

## <a name="sec_07"></a> ARIMA 모델
본 절에서는 statsmodel 라이브러리를 이용하여 시계열 자료에 대한 ARIMA 모형을 구축하고 적합시키는 방법에 대해 설명한다. ARIMA는 자동회귀 통합 이동 평균(AutoRegressive Integrated Moving Average)의 약자로 시계열의 자기상관성(autocorrelation)과 비정상성(non-stationarity)을 포착할 수 있는 모델의 한 종류이다. ARIMA 모델은 시계열의 과거와 현재의 가치를 바탕으로 미래의 가치를 예측하는 데 도움을 줄 수 있다.

ARIMA 모델은 크게 AR, I 및 MA의 세 가지 구성요소로 되어 있다. AR은 AutoRegressive의 약자로 모델이 시계열의 과거 값을 이용하여 현재 가치를 예측하는 것을 의미한다. I는 Integrated의 약자로 모델이 차분을 이용하여 시계열을 정상 상태로 만드는 것을 의미한다. MA는 이동평균(Moving Average)의 약자로 모델이 시계열의 과거 오류를 이용하여 현재 가치를 예측하는 것을 의미한다. ARIMA 모델은 일반적으로 p, d와 q의 세 가지 매개변수로 표시된다. p는 AR 성분의 차수(order), d는 차분의 차수, q는 MA 성분의 차수를 의미한다. 예를 들어 ARIMA(1, 1, 1) 모형은 모형이 하나의 AR 항, 하나의 차분 항, 하나의 MA 항을 가지고 있음을 의미한다.

Python에서 ARIMA 모형을 구축하고 적합시키기 위해서는 statsmodel 라이브러리의 `ARIMA()` 함수를 사용한다. 이 함수는 시계열과 `p`, `d`와 `q` 값을 입력으로 받아 데이터에 적합할 수 있는 ARIMA 모델 객체를 반환한다. 예를 들어 항공 여객 시계열에 대한 ARIMA(1, 1, 1) 모델을 구축하기 위해서는 다음과 같이 코드를 작성할 수 있다.

```python
# Import statsmodels library
import statsmodels.api as sm

# Build an ARIMA(1, 1, 1) model for the time series
model = sm.tsa.ARIMA(df["Passengers"], order=(1, 1, 1))
```

데이터에 모델을 맞추기 위해 모델 개체의 fit() 방법을 사용할 수 있다. 이 방법은 최대우도법(maximum likelyhood method)을 사용하여 모델의 모수를 추정하고 적합치(fitted value), 잔차(residual), 예측치(forecast) 및 기타 정보를 포함하는 결과 객체를 반환한다. 예를 들어 ARIMA(1, 1, 1) 모델을 항공사 승객 시계열에 맞추기 위해 다음과 같이 코드를 작성할 수 있다.

```python
# Fit the model to the data
results = model.fit()
# View the summary of the model
print(results.summary())
```

계수, 표준 오차, 로그 우도(log-likelyhood), AIC 및 진단 테스트(diagnostic test) 같은 모델의 요약을 보여주는 표를 다음과 같이 출력한다.

```
ARIMA Model Results
==============================================================================
Dep. Variable:            D.Passengers   No. Observations:                  143
Model:                 ARIMA(1, 1, 1)   Log Likelihood                -688.747
Method:                       css-mle   S.D. of innovations             29.759
Date:                Tue, 19 Oct 2021   AIC                           1385.495
Time:                        15:03:17   BIC                           1397.601
Sample:                    02-01-1949   HQIC                          1390.265
                         - 12-01-1960
=================================================================================
                   coef    std err          z      P>|z|      [0.025      0.975]
---------------------------------------------------------------------------------
const            2.5308      0.708      3.574      0.000       1.143       3.919
ar.L1.D.Passengers     0.6536      0.089      7.315      0.000       0.478       0.829
ma.L1.D.Passengers    -0.3169      0.100     -3.174      0.002      -0.513      -0.121
                                   Roots
=============================================================================
                  Real          Imaginary           Modulus         Frequency
-----------------------------------------------------------------------------
AR.1            1.5299           +0.0000j            1.5299            0.0000
MA.1            3.1543           +0.0000j            3.1543            0.0000
-----------------------------------------------------------------------------
```

모델의 `const` 항이 2.5308, `AR` 계수가 0.6536, `MA` 계수가 -0.3169임을 알 수 있다. 우리는 또한 모델이 `Log Likelyhood`가 -688.747, `AIC`가 1385.495, `BIC`가 1397.601임을 알 수 있다. AIC와 BIC는 모델의 적합도를 나타내는 척도로 모델의 복잡성과 정확성의 균형을 나타낸다. AIC와 BIC가 낮을수록 모델이 우수함을 나타낸다. 계수의 `p`값이 0.05보다 작음을 알 수 있는데, 이는 통계적으로 유의함을 의미한다.

모델의 성능을 평가하기 위해 결과 객체의 `plot_diagnostics()` 방법을 사용할 수 있다. 이 방법은 유효성과 모델의 가정을 확인하는 데 도움이 될 수 있는 4개의 진단 차트를 플롯한다. 예를 들어 ARIMA(1, 1, 1) 모델의 진단 차트를 플롯하려면 다음과 같이 코드를 작성할 수 있다.

```python
# Plot the diagnostic charts for the model
results.plot_diagnostics(figsize=(10, 8))
plt.show()
```

<span style="color:red">출력 결과를 보일 것</span>

출력에서 진단 차트가 다음과 같은 정보를 보이는 것을 알 수 있다.

- 표준화된 잔차 플롯은 시간에 따른 모델의 잔차를 보인다. 잔차는 대부분 0을 중심으로 하여 뚜렷한 패턴이나 추세를 갖지 않음을 알 수 있다. 이는 모델이 데이터의 대부분의 정보를 포착했으며 잔차가 임의적이고 독립적임을 의미한다.
- 히스토그램과 밀도 플롯은 잔차의 분포를 보인다. 평균이 0이고 표준 편차가 1인 잔차가 대략적으로 정규 분포를 이루고 있음을 알 수 있다. 이는 모형이 잔차의 정규성 가정을 충족했음을 의미한다.
- 정규 Q-Q 플롯은 잔차의 분위수 대 정규 분포의 분위수를 보인다. 우리는 점들이 대부분 45도 선을 따라 정렬되어 있고, 일부 편차는 꼬리에 있음을 알 수 있다. 이것은 잔차가 대부분 정규 분포이고 일부 이상치가 있음을 의미한다.
- 상관도는 잔차의 자기 상관 함수를 보인다. 자기 상관 값이 95% 신뢰 구간을 나타내는 파란색 음영 지역 내에 대부분 있는 것을 볼 수 있다. 이는 잔차가 유의하게 자기 상관이 없으며 모델이 잔차가 자기 상관이 없다는 가정을 충족했음을 의미한다.

이러한 진단 차트를 바탕으로 ARIMA(1, 1, 1) 모델이 항공사 승객 시계열에 타당하고 합리적인 모델이라는 결론을 내릴 수 있다.

본 절에서는 statsmodel 라이브러리를 이용하여 시계열 자료에 대한 ARIMA 모델을 구축하고 적합시키는 방법을 배웠다. ARIMA 모델은 과거와 현재의 가치를 바탕으로 시계열의 미래 가치를 예측하는 데 도움을 줄 수 있다. 다음 절에서는 SARIMAX 클래스를 이용하여 계절별 시계열 자료를 다룰 수 있도록 ARIMA 모델을 확장하는 방법을 설명할 것이다.

## <a name="sec_08"></a> 계절별 ARIMA 모델
## <a name="sec_09"></a> 지수 평활 방법
## <a name="sec_10"></a> Prophet
## <a name="sec_11"></a> 시계열 신경망
## <a name="sec_12"></a> 시계열 예측을 위한 평가지표
## <a name="summary"></a> 마치며
